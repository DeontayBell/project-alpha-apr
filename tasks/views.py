from django.shortcuts import render, redirect
from tasks.forms import TasksForm
from tasks.models import Task
from django.contrib.auth.decorators import login_required

# Create your views here.


def create_task(request):
    if request.method == "POST":
        form = TasksForm(request.POST)
        if form.is_valid():
            task = form.save()
            task.owner = request.user
            task.save()
            return redirect("list_projects")
    else:
        form = TasksForm()
    context = {"form": form}
    return render(request, "tasks/create.html", context)


@login_required
def show_my_task(request):
    tasks = Task.objects.filter(assignee=request.user)
    context = {"tasks": tasks}
    return render(request, "tasks/tasks.html", context)
